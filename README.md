# kLogNLP

kLog ([http://klog.dinfo.unifi.it](http://klog.dinfo.unifi.it)) is a framework for graph kernel-based relational learning, which has already proven successful for a number of tasks in natural language processing. kLogNLP is a natural language processing module for kLog. This module enriches kLog with NLP-specific preprocessors, enabling the use of existing libraries and toolkits within an elegant and powerful declarative machine learning framework.

The official kLogNLP page is:

[http://dtai.cs.kuleuven.be/klognlp](http://dtai.cs.kuleuven.be/klognlp)