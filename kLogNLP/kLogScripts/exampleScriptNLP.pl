/** Copyright (C) 2014 Mathias Verbeke (mathias.verbeke@cs.kuleuven.be)
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
  =============== kLog NLP example script ===============
  Example script for the kLogNLP module 
  
  Mathias Verbeke - Mathias.Verbeke@cs.kuleuven.be
  =======================================================
  
*/

:- use_module(klog).
:- use_module(klognlp).
:- use_module(library(lists)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

begin_domain.

signature sentence(sent_id::self)::extensional.

signature nextS(sent_1::sentence, sent_2::sentence)::extensional.

signature word(word_id::self,
          word_string::property,
          lemma::property,
          postag::property,
          namedentity::property
	      )::extensional.

signature nextW(word_1::word, word_2::word)::extensional.

signature corefPhrase(coref_id::self)::extensional.

signature isPartOfCorefPhrase(coref_phrase::corefPhrase, word::word)::extensional.

signature coref(coref_phrase_1::corefPhrase, coref_phrase_2::corefPhrase)::extensional.

%signature synonymous(word_1::word, word_2::word)::extensional.

signature dependency(word_1::word,
            word_2::word,
            dep_rel::property
           )::extensional.

kernel_points([word]).

end_domain.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dataset :-
	klog_flag(klog_master,verbosity,4),
	new_feature_generator(my_fg,nspdk),
	klognlp_flag(preprocessor,stanfordnlp),
	dataset("/tmp/testkLogNLP/","/tmp/testNLP.pl").

experiment :-
    % kLogNLP
    klognlp_flag(preprocessor, stanfordnlp),
    dataset(’/home/dataset/train/’, ’trainingset.pl’),
    attach(’trainingset.pl’),
    % Kernel parametrization
    new_feature_generator(my_fg, nspdk),
    klog_flag(my_fg, radius, 1),
    klog_flag(my_fg, distance ,1),
    klog_flag(my_fg, match_type , hard),
    % Learner parametrization
    new_model(my_model, libsvm_c_svc),
    klog_flag(my_model, c, 0.1),
    kfold(target , 5, my_model , my_fg).