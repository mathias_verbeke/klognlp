# Copyright (C) 2014 Mathias Verbeke (mathias.verbeke@cs.kuleuven.be)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 2.1 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from kLogNLPException import kLogNLPException
from filePreprocessor import filePreprocessor

class directoryPreprocessor(object):

    def __init__(self, listOfSignatures, wordColumnNames, preprocessor):
        # Features to include (based on kLog signatures)
        self.featsDict = {}
        self.featsDict['sentence'] = False
        self.featsDict['nextS'] = False
        self.featsDict['word'] = False
        self.featsDict['nextW'] = False
        self.featsDict['dependency'] = False
        self.featsDict['synonymous'] = False
        self.featsDict['coref'] = False
        self.featsDict['corefPhrase'] = False
        self.featsDict['isPartOfCorefPhrase'] = False
        # Word features to include (based on kLog signature for word)
        self.wordFeatsDict = {}
        self.wordFeatsDict['word_string'] = False
        self.wordFeatsDict['lemma'] = False
        self.wordFeatsDict['postag'] = False
        self.wordFeatsDict['namedentity'] = False
        if 'sentence' in listOfSignatures:
            self.featsDict['sentence'] = True
        if 'nextS' in listOfSignatures:
            self.featsDict['nextS'] = True
        if 'word' in listOfSignatures:
            self.featsDict['word'] = True
        if 'nextW' in listOfSignatures:
            self.featsDict['nextW'] = True
        if 'dependency' in listOfSignatures:
            self.featsDict['dependency'] = True
        if 'synonymous' in listOfSignatures:
            self.featsDict['synonymous'] = True
        if 'coref' in listOfSignatures:
            self.featsDict['coref'] = True
        if 'corefPhrase' in listOfSignatures:
            self.featsDict['corefPhrase'] = True
        if 'isPartOfCorefPhrase' in listOfSignatures:
            self.featsDict['isPartOfCorefPhrase'] = True
        if 'word_string' in wordColumnNames:
            self.wordFeatsDict['word_string'] = True
        if 'lemma' in wordColumnNames:
            self.wordFeatsDict['lemma'] = True
        if 'postag' in wordColumnNames:
            self.wordFeatsDict['postag'] = True
        if 'namedentity' in wordColumnNames:
            self.wordFeatsDict['namedentity'] = True
        self.preprocessor = preprocessor

    def processDirectory(self, inputDirectoryName, outputFileName):
        listOfFiles = [name for name in os.listdir(inputDirectoryName) if os.path.isfile(os.path.join(inputDirectoryName, name))]
        numberOfFilesInDir = len(listOfFiles)
        documentNumber = 1
        if numberOfFilesInDir == 0:
            raise kLogNLPException("No files in given input directory!")
        else:
            for fileName in listOfFiles:
                fileNameWithoutExt, fileExtension = os.path.splitext(fileName)
                if fileExtension == '.txt':
                    fp = filePreprocessor()
                    fp.processTXTFile(os.path.join(inputDirectoryName,fileName), documentNumber, self.featsDict, self.wordFeatsDict, outputFileName, self.preprocessor)
                    documentNumber += 1
                elif fileExtension == '.xml':
                    fp = filePreprocessor()
                    fp.processXMLFile(os.path.join(inputDirectoryName,fileName), documentNumber, self.featsDict, self.wordFeatsDict, outputFileName, self.preprocessor)
                    documentNumber += 1
                else:
                    raise kLogNLPException("The given input directory contains a non-supported file: " + fileName)
