# Copyright (C) 2014 Mathias Verbeke (mathias.verbeke@cs.kuleuven.be)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 2.1 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import xml.etree.ElementTree as ET
import nltk
from kLogNLPException import kLogNLPException
from sentencePreprocessor import sentencePreprocessor
# Stanford NLP imports
from corenlp import StanfordCoreNLP

class filePreprocessor(object):

    def processTXTFile(self, fileName, documentID, featsDict, wordFeatsDict, outputFileName, preprocessor):
        outputFile = open(outputFileName,'a')
        txtFile = open(fileName, 'r')
        sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
        text = txtFile.read()
        sentenceList = sent_detector.tokenize(text.strip())
        sentenceID = 1
        for sentence in sentenceList:
            interpretationID = self.getInterpretationID(featsDict, documentID, sentenceID)
            outputFile.write("interpretation(" + interpretationID + ",sentence(s" + repr(sentenceID) + ")).\n")
            if (sentenceID > 1) and ('nextS' in featsDict.keys()):
                outputFile.write("interpretation(" + interpretationID + ",nextS(s" + repr(sentenceID-1) + ",s" + repr(sentenceID) + ")).\n")
            self.processSentence(sentence, documentID, sentenceID, interpretationID, featsDict, wordFeatsDict, outputFile, preprocessor)
            sentenceID += 1
        outputFile.close()

    def processXMLFile(self, fileName, documentID, featsDict, wordFeatsDict, outputFileName, preprocessor):
        outputFile = open(outputFileName,'a')
        tree = ET.parse(fileName)
        root = tree.getroot()
        if root.tag == "corpus":
            for document in root:
                if not document.tag == "document":
                    raise kLogNLPException("XML document " + fileName + " is not well formatted; expected document tag. Please reformat the concerned document(s).")
                sentenceID = 1
                for sentence in document:
                    if not sentence.tag == "sentence":
                        raise kLogNLPException("XML document " + fileName + " is not well formatted; expected sentence tag. Please reformat the concerned document(s).")
                    interpretationID = self.getInterpretationID(featsDict, documentID, sentenceID)
                    outputFile.write("interpretation(" + interpretationID + ",sentence(s" + repr(sentenceID) + ")).\n")
                    if (sentenceID > 1) and ('nextS' in featsDict.keys()):
                        outputFile.write("interpretation(" + interpretationID + ",nextS(s" + repr(sentenceID-1) + ",s" + repr(sentenceID) + ")).\n")
                    self.processSentence(sentence.text, documentID, sentenceID, interpretationID, featsDict, wordFeatsDict, outputFile, preprocessor)
                    sentenceID += 1
        else:
            sentenceID = 1
            for sentence in root:
                if not sentence.tag == "sentence":
                    raise kLogNLPException("XML document " + fileName + " is not well formatted; expected sentence tag. Please reformat the concerned document(s).")
                interpretationID = self.getInterpretationID(featsDict, documentID, sentenceID)
                outputFile.write("interpretation(" + interpretationID + ",sentence(s" + repr(sentenceID) + ")).\n")
                if (sentenceID > 1) and ('nextS' in featsDict.keys()):
                    outputFile.write("interpretation(" + interpretationID + ",nextS(s" + repr(sentenceID-1) + ",s" + repr(sentenceID) + ")).\n")
                self.processSentence(sentence.text, documentID, sentenceID, interpretationID, featsDict, wordFeatsDict, outputFile, preprocessor)
                sentenceID += 1
        outputFile.close()

    def processSentence(self, sentence, documentID, sentenceID, interpretationID, featsDict, wordFeatsDict, outputFile, preprocessor):
        sp = sentencePreprocessor()
        if preprocessor == 'nltk':
            sp.processSentenceNLTK(sentence, documentID, sentenceID, interpretationID, featsDict, wordFeatsDict, outputFile)    
        if preprocessor == 'stanfordnlp':
            stanfordNLPDir = os.environ['CORENLPDIR']
            corenlp = StanfordCoreNLP(stanfordNLPDir)
            sp.processSentenceStanfordNLP(sentence, documentID, sentenceID, interpretationID, featsDict, wordFeatsDict, outputFile, corenlp)    
        
    def getInterpretationID(self, featsDict, documentID, sentenceID):
        if 'sentence' in featsDict.keys():
            return "d" + repr(documentID)
        else:
            return "d" + repr(documentID) + "s" + repr(sentenceID)
