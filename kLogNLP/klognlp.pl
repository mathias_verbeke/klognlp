/** Copyright (C) 2014 Mathias Verbeke (mathias.verbeke@cs.kuleuven.be)
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

:- module(klognlp,
	  [dataset/2,
	   %%% module klognlp_flags
	   set_klognlp_flag/2,
	   get_klognlp_flag/2,
	   klognlp_flag/2,
	   klognlp_flags/0,
	   klognlp_flags/1
	  ]).

:- ensure_loaded(klognlp_flags).
:- ensure_loaded(library(lists)).
:- ensure_loaded(library(python)).
:- ensure_loaded(library(system)).

dataset(Directory,FileName) :-
	klog:install_directory(DA),
	format('Identified kLog install dir: ~a~n',[DA]),
	atom_codes(DA,D),
	append(S,[_,_,_,_],D),
	%atom_codes(SA,S),
	%format('kLog install dir without src: ~a~n',[SA]), % DEBUGGING
	append(S,"modules/kLogNLP",N),
	atom_codes(NA,N),
	format('kLogNLP module dir: ~a~n',[NA]),
	:= sys:path:append(NA),	
	python:python_import('nlpinterface'),
	atom_codes(DirectoryA,Directory),
	atom_codes(FileNameA,FileName),
	% Check preprocessor
	klognlp_flags:klognlp_flag(preprocessor,PreprocessorA),
	format('Preprocessor set to: ~a~n',[PreprocessorA]),
	% Start caching mechanism
	tmp_file('klognlp',TmpFile),
	open(TmpFile,write,Stream),
	klog:domain_traits(Stream),
	% Also write preprocessor to file to hash (as the preprocessor is arranged by the kLog flags mechanism)
	write(Stream, PreprocessorA),
	close(Stream),
	(atom_codes(FileNameA,FileName),file_exists(FileNameA) ->
	 Hash := preprocessDomainTraintsCacheAndCheckSameHash(FileNameA, TmpFile),
	 %format('Hash result: ~a~n',[Hash]), % DEBUGGING
	 (\+Hash == -1 ->
	  % Hashes don't match => Preprocess dataset
	  %format('Hash result (should not equal -1): ~a~n',[Hash]), % DEBUGGING
	  klog:domain_traits(extensional_signatures,ExtSignatures),
	  (member('word',ExtSignatures) ->
	   klog:signature_traits('word',column_names,WordColumnNames),
	   := preprocessDirectory(DirectoryA,FileNameA,ExtSignatures,WordColumnNames,Hash,PreprocessorA)
	  ;
	   := preprocessDirectory(DirectoryA,FileNameA,ExtSignatures,[],Hash,PreprocessorA)
	  )
	  ;
	  % Hashes match, nothing needs to be done
	  true
	 )
	;
	 % File does not exist yet
	 Hash := computeFileHash(TmpFile),
	 %format('Hash result (file does not exist): ~a~n',[Hash]), % DEBUGGING
	 klog:domain_traits(extensional_signatures,ExtSignatures),
	 (member('word',ExtSignatures) ->
	  klog:signature_traits('word',column_names,WordColumnNames),
	  := preprocessDirectory(DirectoryA,FileNameA,ExtSignatures,WordColumnNames,Hash,PreprocessorA)
	 ;
	  := preprocessDirectory(DirectoryA,FileNameA,ExtSignatures,[],Hash,PreprocessorA)
	 )
	).