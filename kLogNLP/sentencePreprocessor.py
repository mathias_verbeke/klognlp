# Copyright (C) 2014 Mathias Verbeke (mathias.verbeke@cs.kuleuven.be)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 2.1 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import itertools
from kLogNLPException import kLogNLPException
# NLTK imports
import nltk
from nltk.corpus import wordnet
from nltk.stem.wordnet import WordNetLemmatizer

class sentencePreprocessor(object):

    def processSentenceNLTK(self, sentence, documentID, sentenceID, interpretationID, featsDict, wordFeatsDict, outputFile):
        tokenized = nltk.word_tokenize(sentence)
        posTagged = nltk.pos_tag(tokenized)
        stemmer = nltk.PorterStemmer()
        wordsWithIDDict = {}
        wordID = 1
        namedEntitiesChuncked = nltk.ne_chunk(posTagged)
        NEDict = {}
        for c in namedEntitiesChuncked:
            if hasattr(c, 'node'):
                NEValues = c.leaves()
                label = repr(c).split(",")[0].replace("Tree(","")
                for NEValue in NEValues:
                    NEDict[NEValue] = label
        for posTuple in posTagged:
            wordIDkLog = self.getWordID(wordID, sentenceID, featsDict)
            if (wordID > 1) and ('nextW' in featsDict.keys()):
                outputFile.write("interpretation(" + interpretationID + ",nextW(" + self.getPreviousWordID(wordIDkLog) + "," + wordIDkLog + ")).\n")
            (word,pos) = posTuple
            stem = stemmer.stem(word)
            if word in wordsWithIDDict.keys():
                wordsWithIDDict[word.lower()].append(wordIDkLog)
            else:
                wordsWithIDDict[word.lower()] = []
                wordsWithIDDict[word.lower()].append(wordIDkLog)
            wordString = "word(" + wordIDkLog
            if wordFeatsDict["word_string"]:
                wordString += ",\'" + self.parseStringsForProlog(word) + "\'"
            if wordFeatsDict["lemma"]:
                wordString += ",\'" + self.parseStringsForProlog(stem) + "\'"
            if wordFeatsDict["postag"]:
                wordString += "," + self.parseStringsForProlog(self.parsePOSandNERLabelsForProlog(self.parseDepLabelsForProlog(pos.lower())))
            if wordFeatsDict["namedentity"]:
                if posTuple in NEDict.keys():
                    namedentitytype = NEDict[posTuple].replace("'","").lower()
                else:
                    namedentitytype = "none"
                wordString += "," + self.parsePOSandNERLabelsForProlog(namedentitytype)
            wordString += ")"
            outputFile.write("interpretation(" + interpretationID + "," + wordString + ").\n")
            wordID += 1
        if featsDict["dependency"]:
            raise kLogNLPException("Dependency parsing is currently not supported for the NLTK preprocessor. Please change the preprocessor or remove the dependency signature from the model.")
        if featsDict["coref"]:
            raise kLogNLPException("Coreference resolution is currently not supported for the NLTK preprocessor. Please change the preprocessor or remove the coref signature from the model.")
        if featsDict["synonymous"]:
            synonymousWords = self.checkWordListForSynonyms(wordsWithIDDict)
            for (word1ID,word2ID) in synonymousWords:
                outputFile.write("interpretation(" + interpretationID + ",synonymous(" + word1ID + "," + word2ID + ").\n")

    def processSentenceStanfordNLP(self, sentence, documentID, sentenceID, interpretationID, featsDict, wordFeatsDict, outputFile, corenlp):
        result = corenlp.raw_parse(sentence)
        sentences = result['sentences']
        wordID = 1
        corefPartID = 1
        for sentence in sentences:
            words = sentence['words']
            for wordTuple in words:
                wordIDkLog = self.getWordID(wordID, sentenceID, featsDict)
                if (wordID > 1) and ('nextW' in featsDict.keys()):
                    outputFile.write("interpretation(" + interpretationID + ",nextW(" + self.getPreviousWordID(wordIDkLog) + "," + wordIDkLog + ")).\n")
                (word, wordDict) = wordTuple
                wordString = "word(" + wordIDkLog
                if wordFeatsDict["word_string"]:
                    wordString += ",\'" + self.parseStringsForProlog(word) + "\'"
                if wordFeatsDict["lemma"]:
                    wordString += ",\'" + self.parseStringsForProlog(wordDict['Lemma']) + "\'"
                if wordFeatsDict["postag"]:
                    if wordDict['PartOfSpeech'] == '\'\'':
                        wordDict['PartOfSpeech'] = 'none'
                    wordString += "," + self.parseStringsForProlog(self.parsePOSandNERLabelsForProlog(self.parseDepLabelsForProlog(wordDict['PartOfSpeech'].replace("'","").lower())))
                if wordFeatsDict["namedentity"]:
                    namedentitytype = self.parsePOSandNERLabelsForProlog(wordDict['NamedEntityTag'].lower())
                    if namedentitytype == 'o':
                        namedentitytype = "none"
                    wordString += "," + namedentitytype.replace("'","")
                wordString += ")"
                outputFile.write("interpretation(" + interpretationID + "," + wordString + ").\n")
                wordID += 1
            if featsDict["dependency"] and ('indexeddependencies' in sentence.keys()):
                indexedDependencies = sentence['indexeddependencies']
                for indexedDependency in indexedDependencies:
                    (dependency, word1, word2) = indexedDependency
                    dependency = dependency.replace("'","")
                    splittedWord1 = word1.split("-")
                    word1ID = splittedWord1[len(splittedWord1)-1].replace("'","")
                    word1IDkLog = self.getWordID(word1ID, sentenceID, featsDict)
                    splittedWord2 = word2.split("-")
                    word2ID = splittedWord2[len(splittedWord2)-1].replace("'","")
                    word2IDkLog = self.getWordID(word2ID, sentenceID, featsDict)
                    if dependency == 'root':
                        outputFile.write("interpretation(" + interpretationID + ",dependency(" + word2IDkLog + "," + word2IDkLog + "," + self.parseDepLabelsForProlog(dependency) + ")).\n")
                    else:
                        outputFile.write("interpretation(" + interpretationID + ",dependency(" + word1IDkLog + "," + word2IDkLog + "," + self.parseDepLabelsForProlog(dependency) + ")).\n")
            if featsDict["coref"] and featsDict["corefPhrase"] and featsDict["isPartOfCorefPhrase"] and ('coref' in result.keys()):
                corefLists = result['coref']
                for corefList in corefLists:
                    for corefTuple in corefList:
                        (corefTuplePart1,corefTuplePart2) = corefTuple
                        (word1, sent1, head1, start1, end1) = corefTuplePart1
                        corefIDkLog1 = self.getCorefID(corefPartID, sentenceID, featsDict)
                        corefPartID += 1
                        outputFile.write("interpretation(" + interpretationID + ",corefPhrase(" + corefIDkLog1 + ")).\n")
                        for wordID in range(start1+1, end1+1):
                            wordIDcoref = self.getWordID(wordID, sentenceID, featsDict)
                            outputFile.write("interpretation(" + interpretationID + ",isPartOfCorefPhrase(" + corefIDkLog1 + "," + wordIDcoref + ")).\n")
                        (word2, sent2, head2, start2, end2) = corefTuplePart2
                        corefIDkLog2 = self.getCorefID(corefPartID, sentenceID, featsDict)
                        corefPartID += 1
                        outputFile.write("interpretation(" + interpretationID + ",corefPhrase(" + corefIDkLog2 + ")).\n")
                        for wordID in range(start2+1, end2+1):
                            wordIDcoref = self.getWordID(wordID, sentenceID, featsDict)
                            outputFile.write("interpretation(" + interpretationID + ",isPartOfCorefPhrase(" + corefIDkLog2 + "," + wordIDcoref + ")).\n")
                        outputFile.write("interpretation(" + interpretationID + ",coref(" + corefIDkLog1 + "," + corefIDkLog2 + ")).\n")
            if featsDict["synonymous"]:
                raise kLogNLPException("Synonymy detection is currently not supported for the StanfordNLP preprocessor. Please change the preprocessor or remove the synonymous signature from the model.")
                
    def getCorefID(self, corefID, sentenceID, featsDict):
        if 'sentence' in featsDict.keys():
            return "s" + repr(sentenceID) + "c" + repr(corefID)
        else:
            return "c" + repr(corefID)

    def getWordID(self, wordID, sentenceID, featsDict):
        if 'sentence' in featsDict.keys():
            return "s" + repr(sentenceID) + "w" + repr(wordID).replace("'","")
        else:
            return "w" + repr(wordID).replace("'","")

    def getPreviousWordID(self, wordIDkLog):
        currentWordNumber = int(wordIDkLog.split("w")[1])
        previousWordNumber = currentWordNumber - 1
        return wordIDkLog.split("w")[0] + "w" + repr(previousWordNumber)

    def checkWordListForSynonyms(self, wordsWithIDDict):
        synonymousIDs = []
        words = wordsWithIDDict.keys()
        for word in words:
            wordsMinusWord = words
            wordsMinusWord.remove(word)
            for word2 in wordsMinusWord:
                synonymous = self.SynonymChecker(word, word2)
                if(synonymous):
                    #synonymousIDs.append((wordsWithIDDict[word],wordsWithIDDict[word2]))
                    idsWord = wordsWithIDDict[word]
                    idsWord2 = wordsWithIDDict[word2]
                    for idWord in idsWord:
                        for idWord2 in idsWord2:
                            synonymTuple = (idWord, idWord2)
                            synonymousIDs.append(synonymTuple)
        return synonymousIDs

    def SynonymChecker(self, word1, word2):
        """Checks if word1 and word2 and synonyms. Returns True if they are, otherwise False"""
 
        equivalence = WordNetLemmatizer()
        word1 = equivalence.lemmatize(word1)
        word2 = equivalence.lemmatize(word2)
        
        word1_synonyms = wordnet.synsets(word1)
        word2_synonyms = wordnet.synsets(word2)

        if word1_synonyms == [] or word2_synonyms == []:
            return False
        
        scores = [i.wup_similarity(j) for i, j in list(itertools.product(word1_synonyms, word2_synonyms))]

        # Strict definition of synonymy
        if 1.0 in scores:
            return True
        else:
            return False

        # Alternative synonymy definition (from https://gist.github.com/Slater-Victoroff/5713821)
         # max_index = scores.index(max(scores))
         # best_match = (max_index/len(word1_synonyms), max_index % len(word1_synonyms)-1)
 
         # word1_set = word1_synonyms[best_match[0]].lemma_names
         # word2_set = word2_synonyms[best_match[1]].lemma_names
         # match = False
         # match = [match or word in word2_set for word in word1_set][0]
 
         # return match

    def parsePOSandNERLabelsForProlog(self, inputWord):
        inputWord = inputWord.lower().replace("-","")
        return inputWord

    def parseStringsForProlog(self, inputChar):
        inputChar = inputChar.replace("\'","escsingleq").replace(",","esccomma").replace(".","escpoint").replace("!","escexcl").replace("\"","escdoubleq").replace("%","escperc").replace("(","escbrackopen").replace(")","escbrackclosed").replace(":","esccolon").replace("`","rightquote").replace("?","escquestionmark").replace("*","escstar").replace("-","escdash").replace(";","escsemicolon")
        return inputChar

    def parseDepLabelsForProlog(self, inputChar):
        inputChar = inputChar.lower()
        inputChar = inputChar.replace("prp$", "prps").replace("wp$","wps").replace("conj_+","conjplus")
        return inputChar
