# Copyright (C) 2014 Mathias Verbeke (mathias.verbeke@cs.kuleuven.be)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 2.1 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import hashlib
from directoryPreprocessor import directoryPreprocessor

def preprocessDirectory(inputDirectoryName, outputFileName, listOfSignatures, wordColumnNames, currentHash, preprocessor):
    outputFile = open(outputFileName,'w')
    outputFile.write("% " + currentHash + "\n")
    outputFile.close()
    dp = directoryPreprocessor(listOfSignatures, wordColumnNames, preprocessor)
    dp.processDirectory(inputDirectoryName, outputFileName)

# Remove intensional relations from cache file
def preprocessCacheFile(cacheFileName):
    cacheFile = open(cacheFileName,'r')
    lines = cacheFile.readlines()
    lineCounter = 0
    linesToRemove = []
    for line in lines:
        if "level -> intensional" in line:
            linesToRemove.append(lineCounter-3)
            linesToRemove.append(lineCounter-2)
            linesToRemove.append(lineCounter-1)
            linesToRemove.append(lineCounter)
            linesToRemove.append(lineCounter+1)
            linesToRemove.append(lineCounter+2)
            linesToRemove.append(lineCounter+3)
            linesToRemove.append(lineCounter+4)
            linesToRemove.append(lineCounter+5)
        if "kernel_points" in line:
            linesToRemove.append(lineCounter)
        lineCounter += 1
    cacheFile.close()
    cacheFile = open(cacheFileName,'w')
    lineCounter = 0
    for line in lines:
        if not lineCounter in linesToRemove:
            cacheFile.write(line)
        lineCounter += 1
    cacheFile.close()

def computeFileHash(fileName):
     return hashlib.md5(file(fileName, 'r').read()).hexdigest()

def preprocessDomainTraintsCacheAndCheckSameHash(interpretationsFileName, domainTraitsFileName):
    preprocessCacheFile(domainTraitsFileName)
    hashedDomainTraitsCache = computeFileHash(domainTraitsFileName)
    interpretationsFile = open(interpretationsFileName,'r')
    originalHash = interpretationsFile.readlines()[0].split(' ')[1].strip()
    print "ORIGINAL HASH: " + repr(originalHash)
    if(hashedDomainTraitsCache == originalHash):
        return -1
    else:
        return hashedDomainTraitsCache
