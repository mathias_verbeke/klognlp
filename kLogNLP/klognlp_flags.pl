/**
  Copyright (C) 2013 Fabrizio Costa, Kurt De Grave, Luc De Raedt, Paolo Frasconi
  Adaptation from flags.pl:
  Copyright (C) 2014 Mathias Verbeke (mathias.verbeke@cs.kuleuven.be)
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2.1 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

:- module(klognlp_flags, [set_klognlp_flag/2,
		  get_klognlp_flag/2,
		  klognlp_flag/2,
		  klognlp_flags/0,
                  klognlp_flags/1]).

:- ensure_loaded(library(lists)).

:- discontiguous(flag_traits/4).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simplified interface a la Yap (a bit dangerous if used in tests)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% klognlp_flag(+FlagName:atom,?Val:atom) is det.
%
% If Val is a free variable, unify it with the current value of the
% flag FlagName. Otherwise sets FlagName to Val.
klognlp_flag(FlagName,Val) :- ( var(Val) -> get_klognlp_flag(FlagName,Val) ; set_klognlp_flag(FlagName,Val) ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% set_klognlp_flag(+FlagName:atom,+Val:atom) is det.
%
% Sets FlagName to Val.
set_klognlp_flag(FlagName,Val) :- % Prolog case
	flag_traits(FlagName,_,_,_),
	( check_value(FlagName,Val) ->
	  true
	;
	  atomic_list_concat(['Illegal value >',Val,'< for kLogNLP flag >',FlagName,'<'],
			     Message),
	  throw(error(klog_error,
		      (set_klognlp_flag(FlagName,Val), Message)))
	),
        Old=..[FlagName,_],
        New=..[FlagName,Val],
        retractall(Old),
        assert(New).

check_value(FlagName,Val) :-
	flag_traits(FlagName,_,Checker,_),
	Checker=..[Functor|Args],
	Query=..[Functor|[Val|Args]],
	call(Query).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% get_klognlp_flag(+FlagName:atom,-Val) is semidet.
%
% Unify Val with the current value of the flag FlagName.
get_klognlp_flag(FlagName,Val) :- % Prolog case
	flag_traits(FlagName,_,_,_),
        Query=..[FlagName,Val],
        call(Query).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% klognlp_flags(+Stream) is det.
%
% Write a description of current flags to Stream.
klognlp_flags(Stream) :-
	format(Stream,'-----------------------------------------------------------------------------------~n',[]),
	format(Stream,'Prolog-level flags: (current values in square brackets)~n',[]),
	format(Stream,'set_klognlp_flag(+Flag,+Value) to change a flag~n',[]),
	format(Stream,'get_klognlp_flag(+Flag,-Value) to get its current value~n',[]),
	format(Stream,'klognlp_flag(+Flag,?Value) sets or gets a flag value~n',[]),
	format(Stream,'-----------------------------------------------------------------------------------~n',[]),
	forall( flag_traits(FlagName,_CP,Checker,Description),
		describe_flag(Stream,FlagName, Checker, Description)
	      ),
	format(Stream,'-----------------------------------------------------------------------------------~n',[]),
	format(Stream,'-----------------------------------------------------------------------------------~n',[]).

%% klognlp_flags is det.
%
% Write a description of current flags to current output stream.
klognlp_flags :- klognlp_flags(user_output).

describe_flag(Stream,Flag,Values,Help) :-
        get_klognlp_flag(Flag,Val),
	format(Stream,'~w ~t ~24+[~w]~n~w~n~w~n~n',[Flag,Val,Values,Help]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Below are the actual flags.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prolog flags
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- dynamic preprocessor/1.
flag_traits(preprocessor,
	    prolog,member([nltk,stanfordnlp]),
	    'NLP preprocessor used by kLogNLP').
klognlp_flag(preprocessor,stanfordnlp).